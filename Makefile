# This file is part of mbar.
# Copyright (C) 2016, 2019 Sergey Poznyakoff
#
# Mbar is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Mbar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with mbar.  If not, see <http://www.gnu.org/licenses/>. */

# Required version of Mailutils: major and minor numbers.  If patchlevel is
# needed, set the VPAT variable.
VMAJ = 3
VMIN = 3

# Installation prefix
PREFIX = /usr
BINDIR = $(PREFIX)/bin
MANDIR = $(PREFIX)/share/man

MAN1DIR = $(MANDIR)/man1

# Install program.  Use cp(1) if not available.
INSTALL = install
# Program to make directory hierarchy.
MKHIER  = install -d

# Compiler options
O = -ggdb -Wall

## ################################################################## ## 
## There is nothing to modify below this line                         ##

PACKAGE = mbar
VERSION = 1.0

CFLAGS  = $(O) `mailutils cflags`
LDFLAGS = `mailutils ldflags all sieve`

SOURCES = mbar.c
OBJECTS = $(SOURCES:.c=.o)

mbar: $(OBJECTS)
	cc -ombar $(CFLAGS) $(OBJECTS) $(LDFLAGS)

$(OBJECTS): config.h

clean:
	@rm -f mbar $(OBJECTS) config.h

config.h:
	@mailutils help >/dev/null || exit 1
	@set -- `mailutils info version | sed 's/.*=//;s/\./ /g'`;\
	if test $$# -lt 2; then \
	    echo >&2 "Version of mailutils unknown"; \
	    exit 1; \
	elif test $$1 -lt $(VMAJ) || test $$2 -lt $(VMIN); then \
	    echo >&2 "Mailutils version too old: $$1.$$2, but required $(VMAJ).$(VMIN)"; \
	    exit 1;\
	elif test -n "$(VPAT)" && test $${3:-0} -lt $(VPAT); then \
	    echo >&2 "Mailutils version too old: $$1.$$2$${3+.}$$3, but required $(VMAJ).$(VMIN).$(VPAT)"; \
	    exit 1;\
	else \
	    echo "#define MAILUTILS_VERSION_MAJOR $$1"; \
	    echo "#define MAILUTILS_VERSION_MINOR $$2"; \
	    if test "$$#" -gt 2; then \
	        echo "#define MAILUTILS_VERSION_PATCH $$3"; \
	    else \
	        echo "#define MAILUTILS_VERSION_PATCH 0"; \
            fi; \
	fi > config.h

# ###################
# Installation rules
# ###################
install: install-bin install-man

install-bin: mbar 
	$(MKHIER) $(DESTDIR)$(BINDIR)
	$(INSTALL) mbar $(DESTDIR)$(BINDIR)

install-man:
	$(MKHIER) $(DESTDIR)$(MAN1DIR)
	$(INSTALL) mbar.1 $(DESTDIR)$(MAN1DIR)

# ###################
# Distribution rules
# ###################
DISTDIR   = $(PACKAGE)-$(VERSION)
DISTFILES = mbar.c mbar.1 Makefile NEWS README COPYING

distdir:
	rm -rf $(DISTDIR) && \
	mkdir $(DISTDIR) &&  \
	cp $(DISTFILES) $(DISTDIR)

dist: distdir
	tar cfz $(DISTDIR).tar.gz $(DISTDIR)
	rm -rf $(DISTDIR)

distcheck: dist
	tar xfz $(DISTDIR).tar.gz
	@if $(MAKE) -C $(DISTDIR) $(DISTCHECKFLAGS); then \
	  echo "$(DISTDIR).tar.gz ready for distribution"; \
	  rm -rf $(DISTDIR); \
        else \
          exit 2; \
        fi
