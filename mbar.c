/* This file is part of mbar.
   Copyright (C) 2016, 2019 Sergey Poznyakoff

   Mbar is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Mbar is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with mbar.  If not, see <http://www.gnu.org/licenses/>. */

#include "config.h"
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <mailutils/mailutils.h>
#include <mailutils/nls.h>
#include <sysexits.h>
#include <sys/stat.h>

#define PACKAGE_NAME "mbar"
#define PACKAGE_URL  "https://puszcza.gnu.org.ua/software/mbar"
#define PACKAGE_BUGREPORT "gray@gnu.org.ua"

#if MAILUTILS_VERSION_MINOR > 6
# define RESP_DEPTH(r) ((r)->depth)
#elif MAILUTILS_VERSION_MINOR == 6 && MAILUTILS_VERSION_PATCH == 90
# define RESP_DEPTH(r) ((r)->depth)
#else
# define RESP_DEPTH(r) ((r)->level)
#endif

/* FIXME */
#define _(string) string
#define N_(string) string     
#ifndef MU_APP_INIT_NLS
# define MU_APP_INIT_NLS()
#endif

int verbose;
int max_depth = 0;
int dry_run;
int compile_only;
int locus_option;
char *pattern = "*";
char *sourcedir;
char *destdir;
char *cond;
mu_list_t require_list;
char *require_string;
char *template_file;
char *before_date;
char *mailbox_type;
int keep_option;

#define EX_OK 0

#define FLG_MKDIR       0x01
#define FLG_SHOW_SCRIPT 0x02


void
abend (char const *func, char const *arg, int rc)
{
  mu_diag_funcall (MU_DIAG_ERROR, func, arg, rc);
  abort ();
}

static int
req_size (void *item, void *data)
{
  char *str = item;
  size_t *sz = data;
  *sz += strlen (str) + 3;
  return 0;
}

static int
req_concat (void *item, void *data)
{
  char *str = item;
  char **ptr = data;

  *(*ptr)++ = ',';
  *(*ptr)++ = '"';
  *ptr = mu_stpcpy (*ptr, str);
  *(*ptr)++ = '"';
  return 0;
}

static void
build_requires (void)
{
  if (require_list)
    {
      size_t size = 0;
      char *p;
      
      mu_list_foreach (require_list, &req_size, &size);
      require_string = mu_alloc (size + 1);
      p = require_string;
      mu_list_foreach (require_list, &req_concat, &p);
      *p = 0;
      mu_list_destroy (&require_list);
    }
}

char *prog_template = "require [\"fileinto\"${requires}];\n\
if ${cond} {\n\
    fileinto \"${dest}\";${keep:+keep;}\n\
}\n";

void
read_template_file (void)
{
  if (template_file)
    {
      mu_stream_t ins;
      mu_off_t size;
      int rc;
      
      rc = mu_file_stream_create (&ins, template_file, MU_STREAM_READ);
      if (rc)
	abend ("mu_file_stream_create", template_file, rc);

      mu_stream_size (ins, &size);

      prog_template = mu_alloc (size + 1);

      rc = mu_stream_read (ins, prog_template, size, NULL);
      if (rc)
	abend ("mu_stream_read", template_file, rc);
      prog_template[size] = 0;

      mu_stream_destroy (&ins);
    }
}

static void
_sieve_action_log (mu_sieve_machine_t mach,
		   const char *action, const char *fmt, va_list ap)
{
  size_t uid = 0;
  char *folder = mu_sieve_get_data (mach);
  mu_stream_t stream;
  mu_message_t msg = mu_sieve_get_message (mach);

  mu_sieve_get_diag_stream (mach, &stream);
	  
  mu_message_get_uid (msg, &uid);
  mu_stream_printf (stream, "\033s<%d>\033%c<%d>", MU_LOG_NOTICE,
		    locus_option ? 'O' : 'X', MU_LOGMODE_LOCUS);
  mu_stream_printf (stream, _("%s on %s:%zu"), action, folder, uid);

  if (fmt && strlen (fmt))
    {
      mu_stream_printf (stream, ": ");
      mu_stream_vprintf (stream, fmt, ap);
    }
  mu_stream_printf (stream, "\n");
}

static void
nl (mu_stream_t stream, char const *script)
{
  unsigned line = 0;
  while (*script)
    {
      size_t len = strcspn (script, "\n");
      ++line;
      mu_stream_printf (stream, "%4u: ", line);
      mu_stream_write (stream, script, len, NULL);
      mu_stream_write (stream, "\n", 1, NULL);
      script += len;
      if (*script)
	++script;
    }
}

static int
create_path (char *pathbuf)
{
  struct stat st;
  
  if (stat (pathbuf, &st))
    {
      if (errno == ENOENT)
	{
	  char *p = strrchr (pathbuf, '/');
	  if (p)
	    {
	      int rc;
	      *p = 0;
	      rc = create_path (pathbuf);
	      *p = '/';
	      if (rc)
		return rc;
	    }
	  if (mkdir (pathbuf, 0777))
	    {
	      mu_error (_("%s: mkdir failed: %s"),
			pathbuf, mu_strerror (errno));
	      return errno;
	    }
	}
      else
	{
	  mu_error (_("%s: can't stat: %s"), pathbuf, mu_strerror (errno));
	  return errno;
	}
    }
  else if (!S_ISDIR (st.st_mode))
    {
      mu_error (_("%s: not a directory"), pathbuf);
      return EINVAL;
    }
  return 0;
}
  
static int
make_subdirs (const char *str)
{
  mu_url_t url = NULL;
  char *pathbuf, *p;
  int rc;
  
  if (mu_is_proto (str))
    {
      rc = mu_url_create (&url, str);
      if (rc)
	{
	  mu_diag_funcall (MU_DIAG_ERROR, "mu_url_create", str, rc);
	  return rc;
	}
      mu_url_sget_path (url, &str);
    }

  pathbuf = mu_strdup (str);
  p = strrchr (pathbuf, '/');
  if (!p)
    return 0;
  *p = 0;
  rc = create_path (pathbuf);
  if (rc)
    mu_error (_("%s: failed to create directory"), pathbuf);
  free (pathbuf);
  mu_url_destroy (&url);
  return rc;
}

static mu_sieve_machine_t
build_sieve (char const *folder, int flags)
{
  struct mu_wordsplit ws;
  char const *env[9];
  char *dest;
  int rc;
  mu_sieve_machine_t mach;
  char const *p;
  struct mu_locus_point loc = MU_LOCUS_POINT_INITIALIZER;
  
  p = folder + strlen (sourcedir);
  while (p[0] == '/')
    p++;
  dest = mu_make_file_name (destdir, p);

  if (!dry_run && (flags & FLG_MKDIR))
    {
      if (make_subdirs (dest))
	exit (EX_UNAVAILABLE);
    }
  
  env[0] = "cond";
  env[1] = cond;
  
  env[2] = "dest";
  env[3] = dest;

  env[4] = "requires";
  env[5] = require_string ? require_string : "";

  env[6] = "keep";
  env[7] = keep_option ? "1" : "";
  
  env[8] = NULL;

  ws.ws_env = env;
  rc = mu_wordsplit (prog_template, &ws,
		     MU_WRDSF_NOCMD
		     | MU_WRDSF_ENV
		     | MU_WRDSF_ENV_KV
		     | MU_WRDSF_NOSPLIT
		     | MU_WRDSF_SHOWERR
		     | MU_WRDSF_WARNUNDEF);
  if (rc)
    abort ();

  if (flags & FLG_SHOW_SCRIPT)
    {
      mu_printf ("Sieve script:\n");
      nl (mu_strout, ws.ws_wordv[0]);
    }
  
  /* Sieve interpreter setup. */
  rc = mu_sieve_machine_create (&mach);
  if (rc)
    {
      mu_error (_("cannot initialize sieve machine: %s"), mu_strerror (rc));
      exit (EX_SOFTWARE);
    }
  if (verbose > 1)
    mu_sieve_set_logger (mach, _sieve_action_log);
  mu_sieve_set_environ (mach, "location", "MS");
  mu_sieve_set_environ (mach, "phase", "post");
  mu_sieve_set_data (mach, (void *)folder);
  mu_sieve_set_dry_run (mach, dry_run);

  mu_locus_point_set_file (&loc, "stdin");
  loc.mu_line = 1;
  rc = mu_sieve_compile_text (mach, ws.ws_wordv[0], strlen (ws.ws_wordv[0]),
			      &loc);
  mu_locus_point_deinit (&loc);

  if (rc)
    {
      if (!(flags & FLG_SHOW_SCRIPT))
	{
	  mu_stream_printf (mu_strerr, "%s", _("Failed program was:\n"));
	  nl (mu_strerr, ws.ws_wordv[0]);
	}
      abort ();
    }
  mu_wordsplit_free (&ws);
  free (dest);

  return mach;
}
  
static int
enumfun (mu_folder_t folder, struct mu_list_response *resp, void *data)
{
  mu_sieve_machine_t mach;
  mu_mailbox_t mbox;
  int rc;
  mu_url_t url;
  char const *path;
  struct stat st;
  
  if (!(resp->type & MU_FOLDER_ATTRIBUTE_FILE))
    return 0;

  if (verbose)
    mu_printf ("%c%c %c %4d %s\n",
	       (resp->type & MU_FOLDER_ATTRIBUTE_DIRECTORY) ? 'd' : '-',
	       (resp->type & MU_FOLDER_ATTRIBUTE_FILE) ? 'f' : '-',
	       resp->separator,
	       RESP_DEPTH (resp),
	       resp->name);
  
  rc = mu_mailbox_create_at (&mbox, folder, resp->name);
  if (rc)
    {
      mu_diag_funcall (MU_DIAG_ERROR, "mu_mailbox_create", resp->name, rc);
      return 0;
    }

  rc = mu_mailbox_open (mbox, dry_run ? MU_STREAM_READ : MU_STREAM_RDWR);
  if (rc)
    abend ("mu_mailbox_open", resp->name, rc);

  mu_mailbox_get_url (mbox, &url);
  mu_url_sget_path (url, &path);
  if (geteuid () == 0)
    {
      if (stat (path, &st))
	{
	  mu_diag_funcall (MU_DIAG_ERROR, "stat", path, errno);
	  return 0;
	}
      if (setregid (st.st_gid, 0))
	mu_error (_("%s: failed to switch to gid %lu: %s"),
		  path, (unsigned long)st.st_gid, mu_strerror (errno));
      if (setreuid (st.st_uid, 0))
	mu_error (_("%s: failed to switch to gid %lu: %s"),
		  path, (unsigned long)st.st_gid, mu_strerror (errno));
    }
  
  if (!mailbox_type)
    {
      char const *scheme;
      mu_url_sget_scheme (url, &scheme);
      rc = mu_registrar_set_default_scheme (scheme);
      if (rc)
	mu_diag_funcall (MU_DIAG_ERROR, "mu_registrar_set_default_scheme",
			 scheme, rc);
      if (rc)
	{
	  mu_url_destroy (&url);
	  return 0;
	}
    }

  mach = build_sieve (path, FLG_MKDIR);

  rc = mu_sieve_mailbox (mach, mbox);

  if (!dry_run)
    {
      rc = mu_mailbox_expunge (mbox);
      if (rc)
	mu_diag_funcall (MU_DIAG_ERROR, "mu_mailbox_expunge",
			 resp->name, rc);
    }
  
  mu_sieve_machine_destroy (&mach);
  mu_mailbox_close (mbox);
  mu_mailbox_destroy (&mbox);

  return 0;
}

int
scan_folders (void)
{
  int status;
  mu_folder_t folder;
  mu_list_t flist;
  size_t count;
  
  status = mu_folder_create (&folder, sourcedir);
  if (status)
    {
      mu_diag_funcall (MU_DIAG_ERROR, "mu_folder_create", sourcedir, status);
      return 1;
    }
  
  status = mu_folder_open (folder, MU_STREAM_READ);
  if (status)
    {
      mu_diag_funcall (MU_DIAG_ERROR, "mu_folder_open", sourcedir, status);
      return 1;
    }

  status = mu_folder_enumerate (folder, NULL, pattern, 0, max_depth,
				&flist,	enumfun, NULL);
  
  switch (status)
    {
    case 0:
      if (verbose)
	{
	  mu_list_count (flist, &count);
	  mu_printf (_("number of folders: %zu\n"), count);
	}
      mu_list_destroy (&flist);
      break;
      
    case MU_ERR_NOENT:
      mu_printf (_("No folders matching %s in %s\n"), pattern, sourcedir);
      return EX_OK;

    default:
      mu_error (_("mu_folder_list failed: %s"), mu_strerror (status));
      return EX_UNAVAILABLE;
    }
  return EX_OK;
}

static void
modify_debug_flags (mu_debug_level_t set, mu_debug_level_t clr)
{
  mu_debug_level_t lev;
  
  mu_debug_get_category_level (mu_sieve_debug_handle, &lev);
  mu_debug_set_category_level (mu_sieve_debug_handle, (lev & ~clr) | set);
}

static void
cli_trace (struct mu_parseopt *po, struct mu_option *opt, char const *arg)
{
  modify_debug_flags (MU_DEBUG_LEVEL_MASK(MU_DEBUG_TRACE4), 0);
}

static void
cli_instr (struct mu_parseopt *po, struct mu_option *opt, char const *arg)
{
  modify_debug_flags (MU_DEBUG_LEVEL_MASK(MU_DEBUG_TRACE9), 0);  
}

static void
add_require (const char *arg)
{
  if (!require_list)
    {
      int rc = mu_list_create (&require_list);
      if (rc)
	abend ("mu_list_create", NULL, rc);
      mu_list_set_destroy_item (require_list, mu_list_free_item);
    }
  mu_list_append (require_list, mu_strdup (arg));
}  

static void
cli_require (struct mu_parseopt *po, struct mu_option *opt, char const *arg)
{
  add_require (arg);
}

/* mbar [options] DIR DEST [EXPR] */
struct mu_option mbar_options[] = {
  { "maxdepth", 'm', N_("NUM"), MU_OPTION_DEFAULT,
    N_("limits nesting depth"),
    mu_c_int, &max_depth },
  { "dry-run", 'n', NULL, MU_OPTION_DEFAULT,
    N_("don't do anything: print what would be done"),
    mu_c_incr, &dry_run },
  { "verbose", 'v', NULL, MU_OPTION_DEFAULT,
    N_("verbosely print what is being done"),
    mu_c_incr, &verbose },
  { "trace", 't', NULL, MU_OPTION_DEFAULT,
    N_("sieve trace"),
    mu_c_void, NULL, cli_trace },
  { "instructions", 'i', NULL, MU_OPTION_DEFAULT,
    N_("sieve instruction trace"),
    mu_c_void, NULL, cli_instr },
  { "compile-only", 'c', NULL, MU_OPTION_DEFAULT,
    N_("compile script and exit"),
    mu_c_bool, &compile_only },
  { "dump", 'D', NULL, MU_OPTION_DEFAULT,
    N_("compile script, dump disassembled sieve code to terminal and exit"),
    mu_c_int, &compile_only, NULL, "2" },
  { "pattern", 'p', N_("PAT"), MU_OPTION_DEFAULT,
    N_("archive mailboxes matching pattern"),
    mu_c_string, &pattern },
  { "require", 'r', N_("MODULE"), MU_OPTION_DEFAULT,
    N_("require MODULE in the produced Sieve script"),
    mu_c_string, NULL, cli_require },
  { "template-file", 'f', N_("FILE"), MU_OPTION_DEFAULT,
    N_("read template from FILE"),
    mu_c_string, &template_file },
  { "before-date", 'b', N_("DATE"), MU_OPTION_DEFAULT,
    N_("archive messages received before DATE"),
    mu_c_string, &before_date },
  { "mailbox-type", 0, N_("TYPE"), MU_OPTION_DEFAULT,
    N_("set type of destination mailboxes"),
    mu_c_string, &mailbox_type },
  { "locus", 0, NULL, MU_OPTION_DEFAULT,
    N_("show action locations with verbose output"),
    mu_c_bool, &locus_option },
  { "keep", 'k', NULL, MU_OPTION_DEFAULT,
    N_("don't remove messages after archiving"),
    mu_c_bool, &keep_option },
  { NULL }
}, *options[] = { mbar_options, NULL };

static struct mu_cli_setup cli = {
  .optv = options,
  .prog_doc = N_("mailbox archiver"),
  .prog_args = N_("DIR DEST [EXPR]")
};

static char *capa[] = {
  "debug",
  "mailbox",
  "locking",
  "logging",
  "sieve",
  NULL
};

int
main (int argc, char *argv[])
{
  struct mu_parseopt pohint;
  struct mu_cfg_parse_hints cfhint;

  MU_APP_INIT_NLS ();
  mu_register_local_mbox_formats ();

  pohint.po_flags = 0;

  pohint.po_package_name = PACKAGE_NAME;
  pohint.po_flags |= MU_PARSEOPT_PACKAGE_NAME;

  pohint.po_package_url = PACKAGE_URL;
  pohint.po_flags |= MU_PARSEOPT_PACKAGE_URL;

  pohint.po_bug_address = PACKAGE_BUGREPORT;
  pohint.po_flags |= MU_PARSEOPT_BUG_ADDRESS;

  pohint.po_extra_info = mu_general_help_text;
  pohint.po_flags |= MU_PARSEOPT_EXTRA_INFO;

  pohint.po_version_hook = mu_version_hook;
  pohint.po_flags |= MU_PARSEOPT_VERSION_HOOK;

  pohint.po_negation = "no-";
  pohint.po_flags |= MU_PARSEOPT_NEGATION;

  cfhint.site_file = mu_site_config_file ();
  cfhint.flags = MU_CFHINT_SITE_FILE | MU_CFHINT_PER_USER_FILE;

  mu_cli_capa_register (&mu_cli_capa_sieve);
  mu_cli_ext (argc, argv, &cli, &pohint, &cfhint, capa, NULL, &argc, &argv);
  if (dry_run)
    verbose++;
  
  if (mailbox_type)
    {
      int rc = mu_registrar_set_default_scheme (mailbox_type);
      switch (rc)
	{
	case 0:
	  break;

	case MU_ERR_NOENT:
	  mu_error (_("invalid mailbox scheme: %s"), mailbox_type);
	  exit (EX_USAGE);

	default:
	  mu_diag_funcall (MU_DIAG_ERROR, "mu_registrar_set_default_scheme",
			   mailbox_type, rc);
	  exit (EX_SOFTWARE);
	}
    }
  
  if (template_file)
    read_template_file ();

  if (argc < 2)
    {
      mu_error (_("not enough arguments"));
      exit (EX_USAGE);
    }

  sourcedir = argv[0];
  destdir = argv[1];
  if (argc > 2)
    mu_argcv_join (argc - 2, argv + 2, " ", mu_argcv_escape_no, &cond);

  if (before_date)
    {
      char *timecond;
      time_t tlimit, now = time (NULL);
      
      if (mu_parse_date (before_date, &tlimit, &now))
	{
	  mu_error (_("bad date specification"));
	  exit (EX_USAGE);
	}
      
      add_require ("test-timestamp");
      mu_asprintf (&timecond, "timestamp :before \"Date\" \"%s\"",
		   before_date);
      if (cond)
	{
	  char *temp;
	  mu_asprintf (&temp, "allof(%s,%s)", timecond, cond);
	  free (cond);
	  cond = temp;
	  free (timecond);
	}
      else
	cond = timecond;
    }

  if (!cond)
    cond = "true";
  
  build_requires ();

  if (compile_only)
    {
      mu_sieve_machine_t mach;
      char *tmp = mu_make_file_name (sourcedir, "mbox");
      mach = build_sieve (tmp, verbose ? FLG_SHOW_SCRIPT : 0);
      if (compile_only == 2)
	mu_sieve_disass (mach);
      exit (EX_OK);
    }
  
  return scan_folders ();
}
