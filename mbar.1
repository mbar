.\" This file is part of mbar -*- nroff -*-
.\" Copyright (C) 2016, 2019 Sergey Poznyakoff
.\"
.\" mbar is free software; you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation; either version 3, or (at your option)
.\" any later version.
.\"
.\" mbar is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with mbar.  If not, see <http://www.gnu.org/licenses/>.
.\"
.TH MBAR 1 "August 1, 2019" "MBAR"
.SH NAME
mbar \- mailbox archiver
.SH SYNOPSIS
\fBmbar\fR [\fBOPTION\fR] \fIDIR\fR \fIDEST\fR [\fIEXPR\fR]
.SH DESCRIPTION
Scans directory \fIDIR\fR for mailboxes matching pattern (by default -
\fB*\fR, i.e. all mailboxes).  For each mailbox, messages matching the
supplied criteria are picked and moved to the mailbox of the same
name, located under the directory \fIDEST\fR.  If the destination
mailbox doesn't exist, it will be created with the necessary
intermediate directory, so that directory hierarchy under \fIDEST\fR
is identical to that under \fIDIR\fR.
.PP
When no selection criteria are supplied, \fBmbar\fR moves all messages
in each mailbox.
.PP
The \fB\-b\fR (\fB\-\-before\-date\fR) option instructs \fBmbar\fR to
archive only messages received before the date indicated by its
argument.  A wide variety of date formats are understood.  For a
detailed discussion of them, see
.BR http://mailutils.org/manual/html_section/Date-Input-Formats.html .
.PP
The following example will archive messages received more than one
year ago:
.PP
.EX
mbar -b '1 year ago' /var/mail /var/backups/mail
.EE
.PP
The utility works by creating a \fBSieve\fR script using the supplied
criterion as a condition and by executing this script on each mailbox
encountered.
.PP
More complex criteria can be provided using the \fIEXPR\fR optional
argument.  The value of this argument must be a valid \fBSieve\fR
conditional suitable for use in the \fBif\fR statement.
.PP
The script is built from a template, by
expanding the following macro variables:
.TP
.B cond
Expands to the condition expression
.TP
.B dest
Expands to the name of the destination mailbox.
.TP
.B requires
Expands to a comma-separated list of additional requires needed for
successful compilation of the Sieve script.  Such additional requires
are supplied via the \fB\-r\fR (\fB\-\-require\fR) command line
option.
.TP
.B keep
Expands to \fB1\fR if the \fB\-k\fR (\fB\-\-keep\fR) option is given
and to empty string otherwise.
.PP
The default template script is:
.PP
.EX
require ["fileinto"${requires}];
if ${cond} {
    fileinto "${dest}";${keep:+keep;}
}
.EE
.PP
The \fI-b \fIDATE\fR option is a shortcut for specifying
.PP
.EX
timestamp :before "Date" "\fIDATE\fR"
.EE
.PP
as \fIEXPR\fR.
.SH OPTIONS
.TP
\fB\-b\fR, \fB\-\-before\-date=\fIDATE\fR
Archive messages received before DATE
.TP
\fB\-c\fR, \fB\-\-compile\-only\fR
Compile script and exit
.TP
\fB\-D\fR, \fB\-\-dump\fR
Compile script, dump disassembled sieve code to terminal and exit
.TP
\fB\-f\fR, \fB\-\-template\-file=\fIFILE\fR
Read template from \fIFILE\fR.
.TP
\fB\-i\fR, \fB\-\-instructions\fR
Sieve instruction trace.
.TP
\fB\-\-locus\fR
Show action locations with verbose output.
.TP
\fB\-m\fR, \fB\-\-maxdepth=\fINUM\fR
Limits nesting depth to \fINUM\fR directories.
.TP
\fB\-\-mailbox\-type=\fITYPE\fR
Set type of destination mailboxes.
.TP
\fB\-n\fR, \fB\-\-dry\-run\fR
Don't do anything: print what would be done.
.TP
\fB\-p\fR, \fB\-\-pattern=\fIPAT\fR
Archive mailboxes matching pattern (default is \fB*\fR).
.TP
\fB\-r\fR, \fB\-\-require=\fIMODULE\fR
Require \fIMODULE\fR in the produced Sieve script.
.TP
\fB\-t\fR, \fB\-\-trace\fR
Sieve trace.
.TP
\fB\-v\fR, \fB\-\-verbose\fR
Verbosely print what is being done.
.SH EXIT CODES
The exit codes conform with
.BR /usr/include/sysexits.h :
.PP
.TP
.B 0
Success.
.TP
.BR 64 " (" EX_USAGE )
Command line usage error.
.TP
.BR 69 " (" EX_UNAVAILABLE )
Unable to create mailbox or intermediate directories.
.TP
.BR 70 " (" EX_SOFTWARE )
Internal software error.  Please report if you ever encounter this
exit code.
.TP
.BR 78 " (" EX_CONFIG )
Configuration file error.
.SH "SEE ALSO"
.BR http://mailutils.org/manual/html_chapter/Sieve-Language.html .
.\" The MANCGI variable is set by man.cgi script on Ulysses.
.\" The download.inc file contains the default DOWNLOAD section
.\" for man-based doc pages.
.if "\V[MANCGI]"WEBDOC" \{\
.       ds package mbar
.       ds version 1.0
.       so download.inc
\}
.SH AUTHORS
Sergey Poznyakoff
.SH "BUG REPORTS"
Report bugs to <gray@gnu.org>.
.SH COPYRIGHT
Copyright \(co 2016, 2019 Sergey Poznyakoff
.br
.na
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
.br
.ad
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
.\" Local variables:
.\" eval: (add-hook 'write-file-hooks 'time-stamp)
.\" time-stamp-start: ".TH [A-Z_][A-Z0-9_]* [0-9] \""
.\" time-stamp-format: "%:B %:d, %:y"
.\" time-stamp-end: "\""
.\" time-stamp-line-limit: 20
.\" end:

